node_image.module
README.txt

Description
------------

The node_image module enables the adding of images to Drupal content 
in two ways:
* A single image (both full size and thumbnail versions) can be 
  associated with a node or a taxonomy term--as with a thumbnail to
  accompany a link to the page.  These are called "associated images".
  These images are not displayed directly by node_image but must be
  called by other modules or themes. For examples of implementation,
  see the "gworks" theme.

The module doesn't track image name information in a database.  Rather,
inline images keep their original names and HTML references (<img /> 
elements) are inserted into the node body text.  Associated images are
named in the following form:

  id_format.ext

where "id" is a node or taxonomy term ID value, "type" is the format of
the image (wither "summ" for a thumbnail to accompany a summary of 
the node or term or "full" for a full-size image to accompany the full
listing) and "ext" is the file extension.  For example, the full-size
jpg-format image to accompany the term with an ID of 23 would be named:

  23_full.jpg

Usage
-----

Uploading Images

Associated images can be uploaded in one of two ways: directly through the 
node_image module or through a hook to the node api.

When configured to do so, the module adds file upload fields to node
create and edit forms.

The module also adds links to node and term display and input
interfaces.  For nodes, links are added to displays for uploading
images (look for them next to the "read more" and "administer" links
when you're logged on as a user with appropriate permissions).  For 
terms, a link to upload images is added to the tope of the
confirmation page displayed when a term is added or edited.  In either
case, clicking these node or term links brings up a form with two
file upload fields (one for each format of image).

Displaying Images

The module doesn't directly add associated images to term
displays.  Rather, it provides a method that can be called themes or
other modules.

The image display function, node_image_get_image, can be called from
any other module or theme to return an image element (if the relevant
image file exists).  To handle testing for the module's presence when
using this function, it's probably most handy to include a function
like the following in each module or theme referencing node or term
images:

  function module_or_theme_name_get_img($node,$type,$format){
    if(module_exist("node_image")){
      return node_image_get_image($node,$type,$format);
    }
    else{
      return;
    }
  }

where "module_or_theme_name" is the name of the theme or module.  The 
parameters:
* $node is the node or term object.  If type is node, must include 
  $node->nid and $node->title.  If type is term, must include 
  $term->tid.
* $type is "node" when the image will accompany a node listing, "term"
  where it's accompanying a term
* $format is "summ" for a thumbnail accompanying a summary listing,
  "full" for a (usually) larger image accompanying a full listing.

This way, a simple call in the form

  module_or_theme_name_get_img($node,"node","full")

will return an HTML image element if the referenced image is available.
The returned <img/> element includes correct height and width
information.

Configuration
-------------
 
To configure the module, click administration > configuration > modules
> node_image.  
* Set "Add fields to node forms" to "Enabled" if you wish to enable 
  direct image upload when nodes are added or edited.  (See "install"
  file for instructions on a necessary patch to node.module.)
* Enable "Use terms where no node images" if you wish to have
  associated term images used where there isn't an available node image
  (enabled by default).
* Similarly, enable "Use parent terms where term empty" if you wish to
  have images substituted from containing terms (i.e., further up the
  term hierarchy) where there is no image for the current term
  (enabled by default).
* For "Supported image extensions" put a comma-separated list of image
  file extensions, e.g., ".png,.gif".
* "Image alignment" controls how node or term images will float; put
  "right" for a right float, "left" for a left one.
The rest of the configuration options are for optionally supressing
term images where they won't display properly.  Because where images
float left or right the text wraps around the image, it's desireable
to suppress images when there isn't enough text to wrap around them.
(If you don't, text further down on the page may display directly
over the image.)  The various settings here can be used to tweak the
performance of image suppression--adjust them only if needed.
